import pygame

class GameScreen():
    SCREEN_WIDTH = 500
    SCREEN_HEIGHT = 500
    resolution = (SCREEN_WIDTH, SCREEN_HEIGHT)

    def __init__(self):
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        self.screen.fill((255, 255, 255))
        self.draw_arena()

    def get_resolution(self):
        return self.resolution

    def get_screen(self):
        return self.screen

    def draw_arena(self):
        self.screen_area = pygame.draw.rect(self.screen, (255, 0, 0), pygame.Rect(50, 50, 400, 400))

    def update_screen(self):
        self.screen.fill((255, 255, 255))
        self.draw_arena()

