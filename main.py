import pygame
from src.Player import Player
from src.GameScreen import GameScreen
pygame.init()

game_screen = GameScreen()

running = True

player = Player(50, 50, game_screen.get_screen())
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    player.update_position()
    game_screen.update_screen()

    game_screen.get_screen().blit(player.get_player_surf(), player.get_player_position())
    pygame.display.flip()

pygame.quit()