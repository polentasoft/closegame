from Util import Util

def test_sign():
    assert 0 == Util.sign(0.0)
    assert 0 == Util.sign(0.5)
    assert 0 == Util.sign(-0.5)
    assert 1 == Util.sign(0.9)
    assert 1 == Util.sign(42)
    assert -1 == Util.sign(-0.9)
    assert -1 == Util.sign(-42)