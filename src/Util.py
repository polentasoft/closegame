from typing import NewType
from typing import Union

number = NewType('number', Union[int, float])

class Util():
    @staticmethod
    def sign(a: number) -> number:
        """Linear direction which rounds to -1, 0 or 1
        """
        if a < -0.5:
            return -1
        elif a > 0.5:
            return 1
        else:
            return 0

    @staticmethod
    def lerp(a: number, b: number, t: number) -> number:
        """Linear interpolate on the scale given by a to b, using t as the point on that scale.
        """
        return (1 - t) * a + t * b

    @staticmethod
    def clamp(smaller: number, bigger: number, value: number) -> number:
        """Value between the established range
        """
        return min((max((value, smaller)), bigger))
