import pygame
from .Util import Util

class Player:
    INITIAL_X_POSITION = 50
    INITIAL_Y_POSITION = 50

    _color = (0, 0, 255)
    _speed = 0.1
    _x = INITIAL_X_POSITION
    _y = INITIAL_Y_POSITION
    _sprite = None

    # Give the surface a color to separate it from the background


    def __init__(self, w, h, screen):
        self.player_surf = pygame.Surface((self.INITIAL_X_POSITION, self.INITIAL_Y_POSITION))
        self.player_surf.fill((0, 0, 255))
        self._width = w
        self._height = h
        self._screen = screen

    def update_position(self):
        key = pygame.key.get_pressed()

        self._x += (key[pygame.K_RIGHT] - key[pygame.K_LEFT]) * self._speed
        self._y += (key[pygame.K_DOWN] - key[pygame.K_UP]) * self._speed
        
        self._x = Util.clamp(50, 400, self._x)
        self._y = Util.clamp(50, 400, self._y)

    def get_player_surf(self):
        return self.player_surf

    def get_player_position(self):
        return (self._x, self._y)

    